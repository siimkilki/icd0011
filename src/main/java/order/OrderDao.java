package order;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Order save(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        return findOrderById(order.getId());
    }

    public List<Order> findAll() {
        return em.createQuery("select o from Order o")
                .getResultList();
    }

    public void deleteOrder(Long id) {
        em.createQuery("delete from Order o where o.id = :id")
                .executeUpdate();
    }

    public Order findOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class);
        query.setParameter("id", id);

        return query.getSingleResult();
    }
}
