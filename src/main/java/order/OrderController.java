package order;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    private OrderDao dao;

    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @GetMapping("api/orders")
    public List<Order> getAllOrders() {
        return dao.findAll();
    }

    @GetMapping("api/orders/{id}")
    public Order findOrderById(@PathVariable Long id) {
        return dao.findOrderById(id);
    }

    @PostMapping("api/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid Order order) {
        return dao.save(order);
    }

    @DeleteMapping("api/orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        dao.deleteOrder(id);
    }

}