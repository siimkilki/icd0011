package user;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@RestController
public class UserController {

    private UserDao dao;

    public UserController (UserDao dao) {
        this.dao = dao;
    }

    @GetMapping("/")
    public String frontPage() {
        return "Front page!";
    }

    @GetMapping("/count")
    public String counter(HttpSession session) {

        Object count = session.getAttribute("count");

        count = count instanceof Integer
                ? (Integer) count + 1
                : 0;

        session.setAttribute("count", count);

        return String.valueOf(count);
    }

    @GetMapping("/api/home")
    public String home() {
        return "Api home url";
    }

    @GetMapping("/api/info")
    public String info(Principal principal) {
        String user = "";
        if (principal != null) {
            user = principal.getName();
        }

        return "Current user: " + user;
    }

    @GetMapping("/api/version")
    public void version() {
    }

    @GetMapping("/api/admin/info")
    public String adminInfo(Principal principal) {
        return "Admin user info: " + principal.getName();
    }

    @GetMapping("/api/users")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getUsers() {
        return dao.getUsers();
    }

    @GetMapping("/api/users/{userName}")
    @PreAuthorize("#userName == authentication.name or hasRole('ADMIN')")
    public User getUserByName(@PathVariable String userName,
                              Authentication auth) {
        return dao.getUserByUserName(userName);
    }
}