package user;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public User getUserByUserName(String userName) {
        TypedQuery<User> query = em.createQuery(
                "select u from User u where u.userName = :username",
                User.class);
        query.setParameter("username", userName);

        return query.getSingleResult();
    }

    public List<User> getUsers() {
        return em.createQuery("select u from User u")
                .getResultList();
    }
}
