
INSERT INTO USERS (username, password, enabled, first_name)
VALUES ('user', '$2a$10$LiCXcf6VfMuAEssuadbsy.AumdPBREMbmzzv3clMl1ymk.ecuPpDy', true, 'Anonymous User');
INSERT INTO USERS (username, password, enabled, first_name)
VALUES ('admin', '$2a$10$CCUGSUD35K7zwLxX0OA.4.bglKBa5bmLmzPcCriu2ROGLVjZLH5qa', true, 'Anna Admina');

INSERT INTO AUTHORITIES (username, authority) VALUES ('user', 'USER');
INSERT INTO AUTHORITIES (username, authority) VALUES ('admin', 'ADMIN');
INSERT INTO AUTHORITIES (username, authority) VALUES ('admin', 'USER');
